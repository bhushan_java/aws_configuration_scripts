#!/bin/sh -xv
echo "************************************************"
echo "Start"
echo "************************************************"
cd ~
mkdir downloads
mkdir opt
cd downloads/

echo "--------Downloading--------"
wget http://mirrors.linuxeye.com/jdk/jdk-7u80-linux-x64.tar.gz
wget http://a.mbbsindia.com/tomcat/tomcat-7/v7.0.69/bin/apache-tomcat-7.0.69.tar.gz
wget http://nginx.org/download/nginx-1.8.0.tar.gz
echo "--------Downloads ends--------"

echo "--------Extracting JDK and setting the path parameters--------"
tar -xvf jdk-7u80-linux-x64.tar.gz -C /home/ec2-user/opt
echo "export JAVA_HOME=/home/ec2-user/opt/jdk-7u80-linux-x64" >> ~/.bashrc

echo "--------Extracting NGINX and setting the path parameters--------"
tar -xvf nginx-1.8.0.tar.gz -C /home/ec2-user/opt
echo "export NGINX_HOME=/home/ec2-user/opt/nginx-1.8.0" >> ~/.bashrc


echo "--------Setting PATH variable--------"
echo "export PATH=\$PATH:\$JAVA_HOME/bin:\$NGINX_HOME/bin" >> ~/.bashrc

cd ~

cd opt/



echo 'server { \n listen 80;' > xxx.txt
echo "server_name $COLLEGE.collpoll.com www.$COLLEGE.collpoll.com;" >> xxx.txt


echo 'set $redirect_request 1; \n\n' >> xxx.txt
echo 'if ($http_user_agent ~ "n6x4zgB8se7Wyubfov81-android-DT3lrUKlGiv65PKkHV7H") {' >> xxx.txt
echo 'set $redirect_request 0;\n }' >> xxx.txt
echo 'if ($http_user_agent ~ "n6x4m3tA11Cayubfov8143-iOS-32DT3lrUpth31r0nskHV7H") { \n set $redirect_request 0; \n }' >> xxx.txt
echo 'location / { \n\t proxy_set_header Host               $host; \n\t proxy_set_header X-Real-IP          $remote_addr; \n\t proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for; \n\t proxy_set_header X-Forwarded-Proto  $scheme;' >> xxx.txt

echo " proxy_pass          http://127.0.0.1:$PORT/cp\$request_uri;" >>xxx.txt

echo " proxy_redirect      http://127.0.0.1:$PORT/cp\$request_uri http://$COLLEGE.collpoll.com;" >> xxx.txt


echo ' proxy_read_timeout  3000; \n proxy_intercept_errors on; \n }' >> xxx.txt

echo 'error_page 400 /400.html; \n error_page 401 /401.html; \n error_page 403 /403.html; \n error_page 404 /404.html; \n error_page 405 /405.html; \n error_page 500 /500.html; \n error_page 502 /502.html;' >> xxx.txt

echo 'location = /400.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /401.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /403.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /404.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /500.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /502.html { \n root /etc/nginx/error-pages; \n internal; \n } \n }' >> xxx.txt

echo "\n\n" >> xxx.txt

echo "server { \n listen 443;" >> xxx.txt
echo "server_name $COLLEGE.collpoll.com www.$COLLEGE.collpoll.com;" >> xxx.txt


echo 'set $redirect_request 1;' >> xxx.txt
echo 'if ($http_user_agent ~ "n6x4zgB8se7Wyubfov81-android-DT3lrUKlGiv65PKkHV7H") {' >> xxx.txt
echo 'set $redirect_request 0;\n }' >> xxx.txt
echo 'if ($http_user_agent ~ "n6x4m3tA11Cayubfov8143-iOS-32DT3lrUpth31r0nskHV7H") { \n set $redirect_request 0; \n }' >> xxx.txt
echo 'location / { \n\t proxy_set_header Host               $host; \n\t proxy_set_header X-Real-IP          $remote_addr; \n\t proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for; \n\t proxy_set_header X-Forwarded-Proto  $scheme;' >> xxx.txt

echo "\tproxy_pass          http://127.0.0.1:$PORT/cp\$request_uri;" >>xxx.txt

echo "\tproxy_redirect      http://127.0.0.1:$PORT/cp\$request_uri http://$COLLEGE.collpoll.com;" >> xxx.txt


echo '\tproxy_read_timeout  3000; \n proxy_intercept_errors on; \n }' >> xxx.txt

echo 'error_page 400 /400.html; \n error_page 401 /401.html; \n error_page 403 /403.html; \n error_page 404 /404.html; \n error_page 405 /405.html; \n error_page 500 /500.html; \n error_page 502 /502.html;' >> xxx.txt

echo 'location = /400.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /401.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /403.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /404.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /500.html { \n root /etc/nginx/error-pages; \n internal; \n }\n' >> xxx.txt
echo 'location = /502.html { \n root /etc/nginx/error-pages; \n internal; \n } \n }' >> xxx.txt



source ~/.bashrc

echo "------------END---------------"
