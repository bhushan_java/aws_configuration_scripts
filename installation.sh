#!/bin/sh -xv
echo "************************************************"
echo "Installing jdk"
echo "************************************************"

cd ..
mkdir downloads
cd downloads/
wget http://mirrors.linuxeye.com/jdk/jdk-7u80-linux-x64.tar.gz

echo "****renaming*****"
mv jdk-7u80-linux-x64.tar.gz  jdk1.7.0_79

echo "****moving the JDK in opt folder*****"
cd ..
mkdir opt
mv /home/ec2-user/downloads/jdk1.7.0_79 /home/ec2-user/opt

echo "**** configuring environment variables *****"
echo "export JAVA_HOME=/home/ec2-user/opt/jdk1.7.0_79" >> ~/.bashrc


echo "************************************************"
echo "Installing tomcat"
echo "************************************************"


cd opt

wget http://a.mbbsindia.com/tomcat/tomcat-7/v7.0.69/bin/apache-tomcat-7.0.69.tar.gz
tar -xvf apache-tomcat-7.0.69.tar.gz

echo "export TOMCAT_HOME=/home/ec2-user/opt/apache-tomcat-7.0.69" >> ~/.bashrc


echo "************************************************"
echo "Installing nginx"
echo "************************************************"

wget http://nginx.org/download/nginx-1.8.0.tar.gz
tar -xvf nginx-1.8.0.tar.gz

echo "export NGINX_HOME=/home/ec2-user/opt/nginx-1.8.0" >> ~/.bashrc

mv /home/ec2-user/opt/apache-tomcat-7.0.69.tar.gz /home/ec2-user/downloads/
mv /home/ec2-user/opt/nginx-1.8.0.tar.gz /home/ec2-user/downloads/

cd ..
echo "export PATH=\$PATH:\$JAVA_HOME/bin:\$NGINX_HOME/bin:\$TOMCAT_HOME/bin" >> ~/.bashrc

echo "*********************************************"
echo "Creating folder structure"
echo "*********************************************"

sudo mkdir dbCredentials
sudo touch dbCredentials/localDbPassword.txt
sudo mkdir -p indexes/{feedIndex,peopleIndex,boothIndex,bookIndex}
sudo mkdir -p media/{profile,temp}
sudo mkdir -p temp/{logs_dump,new,war_bkp}
sudo mkdir utils
sudo mkdir dbLogs

source ~/.bashrc
