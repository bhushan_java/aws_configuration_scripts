echo "-----------Start---------"
mkdir $1
cd $1/
wget https://s3-us-west-2.amazonaws.com/collegeskeleton/collegeInstanceCreationSkeleton.tar.gz 
tar -xvf collegeInstanceCreationSkeleton.tar.gz
rm collegeInstanceCreationSkeleton.tar.gz

echo "{\"url\":\"""$2\""",\"database\":\"""$3\""",\"port\":\"3306\",\"username\":\"""$4\""",\"password\":\"""$5\"""}" >dbCredentials/localDbPassword.txt
cd dbCredentials/
dbCredentialPath=$(pwd)
cd ..

mkdir $6
cd $6/
mkdir dblogs
utilityFolder=$(pwd)
cd ..

cd indexes/
indexPath=$(pwd)
cd ..

cd opt/apache-tomcat-7.0.68/conf


echo "MEDIA_FOLDER_PARENT_PATH = $utilityFolder" >> configuration.properties 
echo "feedIndexDirectoryPath = $indexPath/feedindex/">> configuration.properties
echo "peopleIndexDirectoryPath = $indexPath/peopleIndex/">> configuration.properties
echo "boothIndexDirectoryPath = $indexPath/boothIndex/">> configuration.properties
echo "bookIndexDirectoryPath = $indexPath/bookIndex/">> configuration.properties

echo "geoFilePath = $indexPath">> configuration.properties
echo "dbCredentialsPath = $dbCredentialPath/localDbPassword.txt">> configuration.properties  
echo "dbLoggerPath = $utilityFolder/dblogs/">> configuration.properties

echo "--------Your college is being initialized----------"
























