#!/bin/sh -xv
echo "####-----------Start---------####"

printf "Name of the college  --> "
read COLLEGE

printf "Database URL         --> "
read URL

printf "Database Name        --> "
read DBNAME

printf "Username             --> "
read USERNAME

printf "Password:            --> "
read PASSWORD

printf "LOG utility folder   --> "
read UTILITY

printf "Enter the port   --> "
read PORT

echo "-----------Checking database connectivity---------"
if ! mysql -u $USERNAME -p$PASSWORD  -e ";" ;
 then echo "********* Authentication failed  *********"; exit 1;
fi

echo "-----------Access Granted---------"

mkdir $COLLEGE
cd $COLLEGE/
wget https://s3-us-west-2.amazonaws.com/collegeskeleton/collegeInstanceCreationSkeleton.tar.gz 
tar -xvf collegeInstanceCreationSkeleton.tar.gz
rm collegeInstanceCreationSkeleton.tar.gz

echo "{\"url\":\"""$URL\""",\"database\":\"""$DBNAME\""",\"port\":\"3306\",\"username\":\"""$USERNAME\""",\"password\":\"""$PASSWORD\"""}" >dbCredentials/localDbPassword.txt

cd dbCredentials/
dbCredentialPath=$(pwd)
cd ..

mkdir $UTILITY
cd $UTILITY/
mkdir dblogs
utilityFolder=$(pwd)
cd ..

cd indexes/
indexPath=$(pwd)
cd ..

cd opt/apache-tomcat-7.0.68/conf

echo "<Connector port=\"""$PORT\""" protocol=\"HTTP/1.1\" connectionTimeout=\"20000\" redirectPort=\"8443\" />" >> server.xml

echo "MEDIA_FOLDER_PARENT_PATH = $utilityFolder" >> configuration.properties 
echo "feedIndexDirectoryPath = $indexPath/feedindex/">> configuration.properties
echo "peopleIndexDirectoryPath = $indexPath/peopleIndex/">> configuration.properties
echo "boothIndexDirectoryPath = $indexPath/boothIndex/">> configuration.properties
echo "bookIndexDirectoryPath = $indexPath/bookIndex/">> configuration.properties

echo "geoFilePath = $indexPath">> configuration.properties
echo "dbCredentialsPath = $dbCredentialPath/localDbPassword.txt">> configuration.properties  
echo "dbLoggerPath = $utilityFolder/dblogs/">> configuration.properties
cd ~





echo "--------Congrats!!! Your college is being initialized----------"

