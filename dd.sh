echo "-----------Start---------"

printf "Name of the college  --> "
read COLLEGE

printf "Database URL  --> "
read URL

printf "Database Name  --> "
read DBNAME

printf "Username  --> "
read USERNAME

printf "Password:  --> "
read PASSWORD

printf "LOG utility folder  --> "
read UTILITY

echo "-----------Checking database connectivity---------"
while ! mysql -u $USERNAME -p$PASSWORD  -e ";" ; do
       read -p "Can't connect, please retry: " PASSWORD
done

echo "-----------Access Granted---------"

mkdir $COLLEGE
cd $COLLEGE/
wget https://s3-us-west-2.amazonaws.com/collegeskeleton/collegeInstanceCreationSkeleton.tar.gz 
tar -xvf collegeInstanceCreationSkeleton.tar.gz
rm collegeInstanceCreationSkeleton.tar.gz

echo "{\"url\":\"""$URL\""",\"database\":\"""$DBNAME\""",\"port\":\"3306\",\"username\":\"""$USERNAME\""",\"password\":\"""$PASSWORD\"""}" >dbCredentials/localDbPassword.txt




cd dbCredentials/
dbCredentialPath=$(pwd)
cd ..

mkdir $UTILITY
cd $UTILITY/
mkdir dblogs
utilityFolder=$(pwd)
cd ..

cd indexes/
indexPath=$(pwd)
cd ..

cd opt/apache-tomcat-7.0.68/conf


echo "MEDIA_FOLDER_PARENT_PATH = $utilityFolder" >> configuration.properties 
echo "feedIndexDirectoryPath = $indexPath/feedindex/">> configuration.properties
echo "peopleIndexDirectoryPath = $indexPath/peopleIndex/">> configuration.properties
echo "boothIndexDirectoryPath = $indexPath/boothIndex/">> configuration.properties
echo "bookIndexDirectoryPath = $indexPath/bookIndex/">> configuration.properties

echo "geoFilePath = $indexPath">> configuration.properties
echo "dbCredentialsPath = $dbCredentialPath/localDbPassword.txt">> configuration.properties  
echo "dbLoggerPath = $utilityFolder/dblogs/">> configuration.properties

echo "--------Your college is being initialized----------"

