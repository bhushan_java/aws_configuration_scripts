#!/bin/sh -xv

echo "####-----------Start---------####"

echo "configuration credentials"
aws configure

printf "What should be the name of your key  --> "
read KEY

printf "What type of instance do you want (options- nano/micro/small/medium/large only)    --> "
read INSTANCE


echo"---Generating the KEY.PEM (v can also specify the name of the key) in this case its  *devenv-key* ---"
aws ec2 create-key-pair --key-name devenv-key --query 'KeyMaterial' --output text > $KEY.pem

echo "changing the rights of the key generated"
chmod 400 $KEY.pem

echo"---creating the instance in the security group and it will return the instance ID---"
INSTANCEID= $(aws ec2 run-instances --image-id ami-29ebb519 --count 1 --instance-type t2.small --key-name $KEY --security-groups launch-wizard-1 --query 'Instances[0].InstanceId')

echo "-----getting the ip address of the instance created using **INSTANCE ID**  -----"
IPADDRESS= $(aws ec2 describe-instances --instance-ids $INSTANCEID  --query 'Reservations[0].Instances[0].PublicIpAddress')

echo"--get the ip address from the above command and connect it using that by SSH---like $IPADDRESS e.g"
ssh -i $KEY.pem ubuntu@$IPADDRESS

echo "####---------Your instance is up and running with instance ID---------####"
echo $INSTANCEID

